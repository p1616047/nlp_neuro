# README

Toutes les ressources relatives à ce projet (cahier des charges, rapport, abstract et lien vers la vidéo) se trouvent [ici](../../wikis)

## Installation pour linux / MacOs

**Etape 1 :** Installation de Python 3.8 : https://www.python.org/downloads/<br />
**Etape 2 :** se placer à la racine du projet.<br />

**Etape 3 :** Installation des packages nécessaire au projet 
```
pip install -r requirement.txt
```
**Etape 4 :** Installation du modèle français pour Spacy :
```
python -m spacy download fr_core_news_md
```

## Installation pour Windows
Anaconda est une distribution scientifique de Python : c’est-à-dire qu’en installant Anaconda, vous installerez Python, Jupyter Notebook (que nous présenterons plus en détail au prochain chapitre) et des dizaines de packages scientifiques.

Téléchargez la distribution Anaconda correspondant à votre système d’exploitation, en Python version 3 :
```
https://www.anaconda.com/distribution/
```

La méthode la plus simple pour Windows consiste à installer la distribution [Anaconda](https://www.anaconda.com/distribution/). Quelques ressources :<br />

* [Installation d'Anaconda pour Windows](https://docs.anaconda.com/anaconda/install/windows/) (tutoriel officiel)
* [Guide d'installation pour Windows](https://www.datacamp.com/community/tutorials/installing-anaconda-windows) (tutoriel alternatif)
* [Utilisation de pip avec Anaconda pour l'installation de packages Python](https://datatofish.com/how-to-install-python-package-in-anaconda/)

## Utilisation

**En ligne de commande à la racine du projet :**
```
python3 model.py -f <file> [-d <file>] [-m <model>] [-w] [-t]
```

* `-f`, `--file` <fichier> : Chemin vers un fichier Excel contenant des données sur lesquelles appliquer les prédictions<br />
* `-d`, `--data` <fichier> : Chemin vers un fichier Excel contenant les données à utiliser pour l'entraînement des modèles (des données sont fournies par défaut)<br />
* `-m`, `--model` <model> : Modèle à utiliser (`sgdc` par défaut), parmi : `sgdc`, `cnb`, `knn`, `mlp`, `mnb`, `rfc`, `svc`

> Les modèles qui correspondent :<br />
> `sgdc` : Méthodes basées sur les Support Vector Machines (SGD classifieur)<br />
> `cnb` : Méthodes basées sur le théorème de Bayes (Complementary Naive Bayes)<br />
> `knn` : Modèle des k-plus proches voisins (k-Nearest Neighbors)<br />
> `mlp` : Perceptron multicouches (MLP Classifier)<br />
> `mnb` : Méthodes basées sur le théorème de Bayes (Multinomial Naive Bayes)<br />
> `rfc` : Les forêts d’arbres aléatoires (Random Forest Classifier)<br />
> `svc` : Méthodes basées sur les Support Vector Machines (Support Vector Classifier)

* `-h`, `--help` : Affiche l'aide<br />
* `-w`, `--word2vec` : Si spécifié, applique Word2Vec au modèle spécifié. **Attention**, ne fonctionne pas avec les modèles `cnb` et `mnb`<br />
* `-t`, `--train` : Si spécifié, entraîne le modèle sélectionné avant d'effectuer les prédictions. _N.B._ Si cette option n'est pas spécifiée mais que le modèle considéré n'a jamais été entraîné, il le sera automatiquement avant de pouvoir effectuer des prédictions.<br />

À l'issue de la classification, les résultats se trouvent dans le fichier `results.xlsx`

## Conventions pour les fichiers de données

Pour ce qui est des données d'entraînement ou de prédiction, ce programme travaille sur des tableurs au format Excel (`.xlsx`). Il faut néanmoins respecter quelques conventions pour pouvoir les traiter :<br />

Le fichier de données d'entraînement (option `-d`) doit contenir deux feuilles de calcul. Dans la première, sept colonnes, nommées respectivement :<br />

* _Subject\_nb_ (peu importante)
* _Order_ (peu importante)
* _Sequence_ (peu importante)
* _Reference_: Une phrase prononcée par le sujet
* _Descriptor_ (peu importante)
* _Subcategory_ (peu importante)
* _Category_: La catégorie de la phrase

Dans la seconde, une colonne nommée _Categories_ liste les catégories possibles pour la classification.<br />

Le fichier contenant les phrases sur lesquelles appliquer les prédictions (option `-f`) doit contenir une colonne, contenant une phrase par ligne.