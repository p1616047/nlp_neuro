from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import ComplementNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, classification_report
import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC
import argparse
import joblib

import processing as prc
import w2vVectorizer as w2v


def log_results(grid_search, test_data, test_target, val_data, val_target):
    print("************************************** RESULTS **************************************")
    print(f"Best estimator: {grid_search.best_estimator_}")
    print(f"Best score: {grid_search.best_score_} for params {grid_search.best_params_}")

    pred = grid_search.predict(test_data.to_list())
    print(f"Test accuracy: {accuracy_score(test_target.to_list(), pred)}")
    pred2 = grid_search.predict(val_data.to_list())
    print(f"Validation accuracy: {accuracy_score(val_target.to_list(), pred2)}")
    print(classification_report(test_target.to_list(), pred))

########################################################
###################   P L O T  #########################
########################################################

#************* CNB Model *******************************
def plot_cnb_results(results, param_grid):
    score = [float(y) for y in results['mean_test_score']]
    alpha = [float(z) for z in results['param_cnb__alpha']]

    _, plot = plt.subplots(1, 1)
    plot.plot(alpha, score)
    plot.set_title('Complement Naive Bayes', fontsize=20, fontweight='bold')
    plot.set_xlabel('Smoothing', fontsize=16)
    plot.set_ylabel('Mean Test Score', fontsize=16)
    plot.grid('on')

    plt.show()

#************* MLP Model *******************************
def plot_mlp_results(results, param_grid):
    scores = [float(y) for y in results['mean_test_score']]
    score = {}
    hidden_size = {}
    sizes = []

    for solver in param_grid['mlp__solver']:
        score[solver] = {}
        hidden_size[solver] = {}

        for size in results['param_mlp__hidden_layer_sizes']:
            score[solver][len(size)] = {}
            hidden_size[solver][len(size)] = {}

            for activation in param_grid['mlp__activation']:
                score[solver][len(size)][activation] = []
                hidden_size[solver][len(size)][activation] = []
            if len(size) not in sizes:
                sizes.append(len(size))

    for idx, val in enumerate(results['param_mlp__activation']):
        score[results['param_mlp__solver'][idx]][len(results['param_mlp__hidden_layer_sizes'][idx])][val] \
            .append(scores[idx])
        hidden_size[results['param_mlp__solver'][idx]][len(results['param_mlp__hidden_layer_sizes'][idx])][val] \
            .append(results['param_mlp__hidden_layer_sizes'][idx][0])

    fig, axs = plt.subplots(len(sizes), len(param_grid['mlp__solver']), squeeze=False, figsize=(20, len(sizes) * 7))

    i = 0
    for size in sizes:
        j = 0
        for solver in param_grid['mlp__solver']:
            axs[i, j].set_title(str(solver) + " solver for " + str(size) + " hidden layers", fontsize=20,
                                fontweight='bold')
            for k, s in score[solver][size].items():
                axs[i, j].plot(hidden_size[solver][size][k], s, label=str(k))
            axs[i, j].set_xlabel("Hidden Layer Size", fontsize=16)
            axs[i, j].set_ylabel("Mean Score", fontsize=16)
            axs[i, j].legend(loc="best", fontsize=15)
            axs[i, j].grid('on')
            j += 1
        i += 1

    plt.savefig("foo.jpeg")
    # plt.show()

#************* MNB Model *******************************
def plot_mnb_results(results, param_grid):
    score = [float(y) for y in results['mean_test_score']]
    alpha = [float(z) for z in results['param_mnb__alpha']]

    _, plot = plt.subplots(1, 1)
    plot.plot(alpha, score)
    plot.set_title('Multinomial Naive Bayes', fontsize=20, fontweight='bold')
    plot.set_xlabel('Smoothing', fontsize=16)
    plot.set_ylabel('Mean Test Score', fontsize=16)
    plot.grid('on')

    plt.show()

#************* RFC Model *******************************
def plot_rfc_results(results, x_param, y_param, x_param_name, y_param_name, plot_title):
    res = [float(y) for y in results[y_param]]
    points = [float(x) for x in results[x_param]]

    _, plot = plt.subplots(1, 1)
    plot.plot(points, res)
    plot.set_title(plot_title, fontsize=20, fontweight='bold')
    plot.set_xlabel(x_param_name, fontsize=16)
    plot.set_ylabel(y_param_name, fontsize=16)
    plot.grid('on')

    plt.show()

#************* SVC Model *******************************
def plot_svc_results(results, param_grid):
    scores = [float(y) for y in results['mean_test_score']]
    score = {}
    c = {}
    poly = {}
    c_poly = {}
    _, axs = plt.subplots(1, 2, figsize=(18, 6))

    for val in param_grid['svc__degree']:
        poly[val] = []
        c_poly[val] = []

    for entry in param_grid['svc__kernel']:
        if entry != 'poly':
            score[entry] = []
            c[entry] = []

    for idx, val in enumerate(results['param_svc__kernel']):
        if val == 'poly':
            poly[results['param_svc__degree'][idx]].append(scores[idx])
            c_poly[results['param_svc__degree'][idx]].append(results['param_svc__C'][idx])
        elif (results['param_svc__degree'][idx] == param_grid['svc__degree'][-1]) and val != 'poly':
            score[val].append(scores[idx])
            c[val].append(results['param_svc__C'][idx])

    for key in score:
        axs[0].plot(c[key], score[key], label=str(key))

    for deg in poly:
        axs[1].plot(c_poly[deg], poly[deg], label='Degree ' + str(deg))

    axs[0].set_title("SVC Search Scores", fontsize=20, fontweight='bold')
    axs[0].set_xlabel("Regularization (C)", fontsize=16)
    axs[0].set_ylabel("Mean Score", fontsize=16)
    axs[0].legend(loc="best", fontsize=15, title="Kernel")
    axs[0].grid('on')

    axs[1].set_title("Evolution of Polynomial Kernel", fontsize=20, fontweight='bold')
    axs[1].set_xlabel("Regularization (C)", fontsize=16)
    axs[1].set_ylabel("Mean Score", fontsize=16)
    axs[1].legend(loc="best", fontsize=15)
    axs[1].grid('on')

    plt.show()

########################################################
##################   M O D E L  ########################
########################################################
def cnb_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    cnb = ComplementNB()
    param_grid = {'cnb__alpha': np.arange(0.001, 1.001, 0.001)}
    cnb_pipe = Pipeline([('tfidf', TfidfVectorizer()), ('cnb', cnb)])
    grid_cnb = GridSearchCV(cnb_pipe, param_grid=param_grid, verbose=10, cv=3)
    grid_cnb.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_cnb, 'cnb')
    log_results(grid_cnb, d_test, l_test, d_val, l_val)
    plot_cnb_results(grid_cnb.cv_results_, param_grid)
    return grid_cnb

def knn_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    KNN = KNeighborsClassifier(n_jobs=2)
    param_grid = {'KNN__n_neighbors': [1, 2, 3, 4, 5],
                  'KNN__weights': ['uniform', 'distance'],
                  'KNN__p': [1, 2]}
    knn_pipe = Pipeline([("tfidf", TfidfVectorizer()), ('KNN', KNN)])
    grid_knn = GridSearchCV(knn_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_knn.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_knn, 'knn')
    log_results(grid_knn, d_test, l_test, d_val, l_val)
    return grid_knn

def knn_w2v_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    KNN = KNeighborsClassifier(n_jobs=2)
    param_grid = {'KNN__n_neighbors': [1, 2, 3, 4, 5],
                  'KNN__weights': ['uniform', 'distance'],
                  'KNN__p': [1, 2]}
    knn_pipe = Pipeline([("w2v", w2v.Word2VecVectorizer()), ('KNN', KNN)])
    grid_knn = GridSearchCV(knn_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_knn.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_knn, 'knn_w2v')
    log_results(grid_knn, d_test, l_test, d_val, l_val)
    return grid_knn

def mlp_model(preprocessor):
    # hidden_layer_sizes: tuple de type (100, 150, 25). Longueur du tuple = nb de couches cachées
    # valeurs = nb neurones pour couche i.
    # (x0, x1, ..., x_i), i = nb Couches, x_i = nb neurones pour couche i

    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    # Construction of the list of hidden layers to try
    # tab1 = [(x,) for x in range(10, 105, 5)]
    # tab2 = [(y, y) for y in range(10, 105, 5)]

    # Construction of the cartesian list of hidden layers to try
    values = [v for v in range(50, 105, 5)]
    tab1 = [(a,) for a in values]
    tab2 = [(a, b) for a in values for b in values]
    # tab3 = [(a, b, c) for a in values for b in values for c in values]

    mlp = MLPClassifier(verbose=False, max_iter=500)
    param_grid = {'mlp__hidden_layer_sizes': tab1 + tab2,
                  'mlp__activation': ['logistic', 'relu', 'tanh'],
                  'mlp__solver': ['adam']}  # ['sgd', 'adam']
    mlp_pipe = Pipeline([('tfidf', TfidfVectorizer()), ('mlp', mlp)])
    grid_mlp = GridSearchCV(mlp_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_mlp.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_mlp, 'mlp')
    log_results(grid_mlp, d_test, l_test, d_val, l_val)
    plot_mlp_results(grid_mlp.cv_results_, param_grid)
    return grid_mlp

def mlp_w2v_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()
    mlp = MLPClassifier(verbose=False, max_iter=500)

    values = [v for v in range(50, 105, 5)]
    tab1 = [(a,) for a in values]
    tab2 = [(a, b) for a in values for b in values]

    param_grid = {'mlp__hidden_layer_sizes': tab1,
                  'mlp__activation': ['logistic', 'relu', 'tanh'],
                  'mlp__solver': ['adam', 'sgd']}
    mlp_pipe = Pipeline([("w2v", w2v.Word2VecVectorizer()), ("mlp", mlp)])
    grid_mlp = GridSearchCV(mlp_pipe, param_grid=param_grid, verbose=10, cv=3)
    grid_mlp.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_mlp, 'mlp_w2v')
    log_results(grid_mlp, d_test, l_test, d_val, l_val)
    return grid_mlp

def mnb_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    mnb = MultinomialNB()
    param_grid = {'mnb__alpha': np.arange(0.001, 1.001, 0.001)}
    mnb_pipe = Pipeline([('tfidf', TfidfVectorizer()), ('mnb', mnb)])
    grid_mnb = GridSearchCV(mnb_pipe, param_grid=param_grid, verbose=10, cv=3)
    grid_mnb.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_mnb, 'mnb')
    log_results(grid_mnb, d_test, l_test, d_val, l_val)
    plot_mnb_results(grid_mnb.cv_results_, param_grid)
    return grid_mnb

def rfc_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    RFC = RandomForestClassifier()
    param_grid = {'RFC__n_estimators': np.arange(10, 510, 10).tolist(),
                  'RFC__n_jobs': [4]}
    rfc_pipe = Pipeline([("tfidf", TfidfVectorizer()), ('RFC', RFC)])
    grid_rfc = GridSearchCV(rfc_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_rfc.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_rfc, 'rfc')
    log_results(grid_rfc, d_test, l_test, d_val, l_val)
    plot_rfc_results(grid_rfc.cv_results_, 'param_RFC__n_estimators', 'mean_test_score', \
                     "N Estimators", "Mean Score", "Grid Search Scores")
    return grid_rfc

def rfc_w2v_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    RFC = RandomForestClassifier()
    param_grid = {'RFC__n_estimators': np.arange(10, 510, 10).tolist(),
                  'RFC__n_jobs': [4]}
    rfc_pipe = Pipeline([("w2v", w2v.Word2VecVectorizer()), ('RFC', RFC)])
    grid_rfc = GridSearchCV(rfc_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_rfc.fit(d_train.to_list(), l_train.to_list())

    save_best_estimator(grid_rfc, 'rfc_w2v')
    log_results(grid_rfc, d_test, l_test, d_val, l_val)
    plot_rfc_results(grid_rfc.cv_results_, 'param_RFC__n_estimators', 'mean_test_score', \
                     "N Estimators", "Mean Score", "Grid Search Scores")
    return grid_rfc

def sgdc_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    sgd = SGDClassifier(n_jobs=2, max_iter=5000)
    param_grid = {'sgd__loss': ['log', 'hinge', 'perceptron', 'squared_hinge', 'modified_huber'],
                  'sgd__l1_ratio': np.arange(0, 0.15, 0.05).tolist()}#np.arange(0, 1.05, 0.05).tolist()
    sgd_pipe = Pipeline([('tfidf', TfidfVectorizer()), ('sgd', sgd)])
    grid_sgd = GridSearchCV(sgd_pipe, param_grid=param_grid, verbose=10, cv=3)
    grid_sgd.fit(d_train.to_list(), l_train)

    save_best_estimator(grid_sgd, 'sgdc')
    log_results(grid_sgd, d_test, l_test, d_val, l_val)
    return grid_sgd

def sgdc_w2v_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    sgd = SGDClassifier(n_jobs=2, max_iter=5000)
    param_grid = {'sgd__loss': ['log', 'hinge', 'perceptron', 'squared_hinge', 'modified_huber'],
                  'sgd__l1_ratio': np.arange(0, 1.05, 0.05).tolist()}
    sgd_pipe = Pipeline([("w2v", w2v.Word2VecVectorizer()), ('sgd', sgd)])
    grid_sgd = GridSearchCV(sgd_pipe, param_grid=param_grid, verbose=10, cv=3)
    grid_sgd.fit(d_train.to_list(), l_train)

    save_best_estimator(grid_sgd, 'sgd_w2v')
    log_results(grid_sgd, d_test, l_test, d_val, l_val)
    return grid_sgd

def svc_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    svc = SVC()
    param_grid = {'svc__C': np.arange(0.5, 3.6, 0.1).tolist(),
                  'svc__degree': [0, 1, 2, 3, 4],
                  'svc__kernel': ['linear', 'poly', 'sigmoid']}
    svc_pipe = Pipeline([('tfidf', TfidfVectorizer()), ('svc', svc)])
    grid_svc = GridSearchCV(svc_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_svc.fit(d_train.to_list(), l_train)

    save_best_estimator(grid_svc, 'svc')
    log_results(grid_svc, d_test, l_test, d_val, l_val)
    plot_svc_results(grid_svc.cv_results_, param_grid)
    return grid_svc

def svc_w2v_model(preprocessor):
    d_train, l_train, d_test, l_test, d_val, l_val = preprocessor.create_splits()

    svc = SVC()
    param_grid = {'svc__C': np.arange(0.5, 3.6, 0.1).tolist(),
                  'svc__degree': [0, 1, 2, 3, 4],
                  'svc__kernel': ['linear', 'poly', 'sigmoid']}
    svc_pipe = Pipeline([("w2v", w2v.Word2VecVectorizer()), ('svc', svc)])
    grid_svc = GridSearchCV(svc_pipe, param_grid=param_grid, verbose=10, cv=2)
    grid_svc.fit(d_train.to_list(), l_train)

    save_best_estimator(grid_svc, 'svc_w2v')
    log_results(grid_svc, d_test, l_test, d_val, l_val)
    plot_svc_results(grid_svc.cv_results_, param_grid)
    return grid_svc

def save_best_estimator(grid, name):
    joblib.dump(grid.best_estimator_, r'models/' + name + '.pkl', compress=1)

def main():
    parser = argparse.ArgumentParser(description='NLP model prediction for xlsx file')
    parser.add_argument('-f', '--file', type=str,
        help='File containing data on which to apply predictions', required=True)
    parser.add_argument('-d', '--data', type=str,
        help='File containing the data to train models on.',
        required=False, default='NLP-data.xlsx')
    parser.add_argument('-m', '--model', type=str, 
        help='Specifies the model to use in : \{sgdc, cnb, '\
        'knn, mlp, mnb, rfc, svc\}', required=False, default='sgdc')
    parser.add_argument('-w', '--word2vec', 
        help='Enables the use of Word2Vec with the selected model.' \
        'WARNING: \'cnb\' and \'mnb\' models don\'t work with Word2Vec', 
        required=False, default=False, action='store_true')
    parser.add_argument('-t', '--train',
        help='Train the model on the specified training data before performing predictions',
        required=False, default=False, action='store_true')
    
    args = parser.parse_args()
    p = prc.Preprocessor(args.data)

    if args.data != 'NLP-data.xlsx':
        p.regen_data()

    if args.model == 'sgdc':
        if args.word2vec:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/sgdc_w2v.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = sgdc_w2v_model(p)
            else:
                grd_srch = sgdc_w2v_model(p)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/sgdc.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = sgdc_model(p)
            else:
                grd_srch = sgdc_model(p)
    elif args.model == 'cnb':
        if args.word2vec:
            print("Error: Word2Vec isn't supported by the cnb model.")
            exit(1)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/cnb.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = cnb_model(p)
            else:
                grd_srch = cnb_model(p)
    elif args.model == 'knn':
        if args.word2vec:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/knn_w2v.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = knn_w2v_model(p)
            else:
                grd_srch = knn_w2v_model(p)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/knn.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = knn_model(p)
            else:
                grd_srch = knn_model(p)
    elif args.model == 'mlp':
        if args.word2vec:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/mlp_w2v.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = mlp_w2v_model(p)
            else:
                grd_srch = mlp_w2v_model(p)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/mlp.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = mlp_model(p)
            else:
                grd_srch = mlp_model(p)
    elif args.model == 'mnb':
        if args.word2vec:
            print("Error: Word2Vec isn't supported by the mnb model.")
            exit(1)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/mnb.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = mnb_model(p)
            else:
                grd_srch = mnb_model(p)
    elif args.model == 'rfc':
        if args.word2vec:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/rfc_w2v.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = rfc_w2v_model(p)
            else:
                grd_srch = rfc_w2v_model(p)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/rfc.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = rfc_model(p)
            else:
                grd_srch = rfc_model(p)
    elif args.model == 'svc':
        if args.word2vec:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/svc_w2v.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = svc_w2v_model(p)
            else:
                grd_srch = svc_w2v_model(p)
        else:
            if not args.train:
                try:
                    grd_srch = joblib.load(r'models/svc.pkl')
                    print("[INFO] Model already trained.")
                except:
                    print("The model hasn't been trained yet. Training anyways.")
                    grd_srch = svc_model(p)
            else:
                grd_srch = svc_model(p)
    else:
        print(f"Error: {args.model} is not a supported model or doesn't exist.")
        exit(1)

    p.get_sentence_prediction(grd_srch, args.file)

if __name__ == '__main__':
    main()