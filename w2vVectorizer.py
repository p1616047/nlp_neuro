from gensim.models import KeyedVectors
import numpy as np

class Word2VecVectorizer():
    def __init__(self):
        self.w2v = KeyedVectors.load_word2vec_format(
            "frWac_postag_no_phrase_1000_skip_cut100.bin", binary=True,
            unicode_errors="ignore")

        self.dim = self.w2v.vector_size

    def fit(self, X, y):
        return self

    def transform(self, X):
        # s are the sentences of the dataset
        for s in range(len(X)):
            # Each cell of X contains a sentence that is now splitted
            # in tokens. We need to transform each token in its embedding.
            X[s] = X[s].split(' ')

            for w in range(len(X[s])):
                # if the word is not part of Word2Vec's vocabulary, replace it
                # by a vector of zeroes the same size as the embedding vectors (
                # essentially weighting nothing in the mean calculation)
                X[s][w] = self.w2v[X[s][w]] if X[s][w] in self.w2v else np.zeros(self.dim)

            #X[s] = [(1 / (len(X[s]) * self.dim)) * np.sum(X[s])]
            X[s] = np.array(np.mean(X[s], axis=0))
        
        return np.array(X)

if __name__ == '__main__':
    w2v = Word2VecVectorizer()
    print(f"The vectors are of size {w2v.dim}.")
    print(w2v.transform(['odeur Noël petit', 'voir grand chose', 'odeur alcool']))