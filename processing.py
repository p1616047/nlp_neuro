import spacy
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

class Preprocessor:
    def __init__(self, datafile):
        self.dataset = None
        self.labels = None
        self.data_file = r'{}'.format(datafile)

        try :
            self.dataset=pd.read_pickle("./dataset.pkl")
            self.labels=pd.read_pickle("./labels.pkl")
        except:
            self.process()
            pd.to_pickle(self.dataset, "./dataset.pkl")
            pd.to_pickle(self.labels, "./labels.pkl")

    def create_labels(self, file):
        labels_df = pd.read_excel(file, sheet_name=1, usecols=[2])
        labels_df.dropna(inplace=True) # Drop empty rows

        labels_encoder = LabelEncoder()
        labels_df['label'] = pd.DataFrame(labels_encoder.fit_transform(labels_df['Categories']))

        self.labels = labels_df

    def create_dataset(self, file):
        data_df = pd.read_excel(file, usecols=[3, 6])
        data_df.dropna(inplace=True) # Drop empty rows
        self.dataset = data_df

    def regen_data(self):
        self.process()
        pd.to_pickle(self.dataset, "./dataset.pkl")
        pd.to_pickle(self.labels, "./labels.pkl")

    def process(self):
        #self.create_labels(r'NLP-data.xlsx') # os.abspath(args.file)
        self.create_labels(self.data_file)
        #self.create_dataset(r'NLP-data.xlsx')
        self.create_dataset(self.data_file)
        nlp = spacy.load('fr_core_news_md')

        # Tokenization, Lemmatization, stop-words and punctuation removal
        j = self.dataset.columns.get_loc('Reference')
        for i in self.dataset.index:
            temp = nlp(self.dataset.iat[i, j])
            self.dataset.iat[i, j] = [t.lemma_ for t in temp if not t.is_stop and not t.is_punct]
            self.dataset.iat[i, j] = ' '.join(self.dataset.iat[i, j])

        self.dataset = pd.merge(self.dataset, self.labels, left_on='Category', right_on='Categories')
        self.dataset.drop(columns=['Category', 'Categories'], inplace=True)

    def get_labels(self):
        return self.labels

    def get_dataset(self):
        return self.dataset

    def create_splits(self):
        # Train: ~60% (8677)
        # Test: ~20% (2712)
        # Validation: ~20% (2170)
        d_temp, d_test, l_temp, l_test \
            = train_test_split(self.dataset['Reference'],\
            self.dataset['label'], test_size=0.2, random_state=22, shuffle=True)

        d_train, d_val, l_train, l_val \
            = train_test_split(d_temp, l_temp, test_size=0.2, random_state=22, shuffle=True)

        return (d_train, l_train, d_test, l_test, d_val, l_val)

    def get_sentence_prediction(self, grid_search, file):
        print("[INFO] Starting prediction.")
        file_name = r'{}'.format(file)
        df = pd.read_excel(file_name)  # Read Excel file as a DataFrame
        nlp = spacy.load('fr_core_news_md')
        df['Prediction']=["None"] * len(df.index)
        j_pred = df.columns.get_loc('Prediction')
        for i in df.index:
            categorie=(grid_search.predict([df.iat[i,0]]))[0]
            df.iat[i, j_pred] = (self.labels.Categories[self.labels.label == categorie].values[0])
        df.to_excel("./result.xlsx")
        print("[INFO] Prediction done.")

if __name__ == '__main__':
    p = Preprocessor()

    #print(p.get_labels())
    #print(p.get_dataset())

    d_train, l_train, d_test, l_test, d_val, l_val = p.create_splits()
    #print(d_train)
    #print(l_train)